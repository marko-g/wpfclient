﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {
        public LogInWindow()
        {
            InitializeComponent();
            Rect workArea = System.Windows.SystemParameters.WorkArea;
            this.Left = (workArea.Width - this.Width) / 2 + workArea.Left;
            this.Top = (workArea.Height - this.Height) / 2 + workArea.Top;
        }

        private void BtnLogin(object sender, RoutedEventArgs e)
        {
            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                data["Name"] = textBoxName.Text;
                data["Password"] = textBoxPassword.Text;

                var response = wb.UploadValues("https://localhost:44336/api/customers/login", "POST", data);
                string responseInString = Encoding.UTF8.GetString(response);


                var jsonresp = JsonConvert.DeserializeObject(responseInString);
                var userObj = JObject.Parse(jsonresp.ToString());
                var userBool = Convert.ToString(userObj["Bool"]);
           

                if (userBool == "True")
                {
                    var Id = Convert.ToString(userObj["Id"]);
                    var name = Convert.ToString(userObj["Name"]);
                    AddQuantity w3 = new AddQuantity(Id,name);
                    w3.Show();
                    this.Close();
                }
                else
                {
                    ErrorWindow errorWindow = new ErrorWindow(Convert.ToString(userObj["Error"]));
                    errorWindow.Show();
                    this.Close();
                }

            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            MainWindow window = new MainWindow();
            window.Show();
            this.Close();
        }
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for Window3.xaml
    /// </summary>
    public partial class AddQuantity : Window
    {
        string number;
      
        public AddQuantity(string num = null, string name = null)
        {
            InitializeComponent();
            number = num;
            nameLabel.Content = name;
            Rect workArea = System.Windows.SystemParameters.WorkArea;
            this.Left = (workArea.Width - this.Width) / 2 + workArea.Left;
            this.Top = (workArea.Height - this.Height) / 2 + workArea.Top;

        }



        private void btnAdd(object sender, RoutedEventArgs e)
        {
            using (var wb = new WebClient())
            {
                var data = new NameValueCollection();
                data["Quantity"] = textBox.Text;


                var response = wb.UploadValues("https://localhost:44336/api/customers/" + number, "PUT", data);
                string responseInString = Encoding.UTF8.GetString(response);


                var jsonresp = JsonConvert.DeserializeObject(responseInString);
                var userObj = JObject.Parse(jsonresp.ToString());
                var quantity = Convert.ToString(userObj["Quantity"]);

                largeTextBox.Text = "Adding until now " + quantity;

            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            LogInWindow logInWindow = new LogInWindow();
            logInWindow.Show();
            this.Close();

        }
    }
}
